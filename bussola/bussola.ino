#include <LiquidCrystal.h>
#include <Servo.h>

Servo ponteiro;
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

#define REFERENCIAS 29

byte grau[] = {
  B11100,
  B10100,
  B11100,
  B00000,
  B00000,
  B00000,
  B00000,
  B00000
};

byte rdv1[] = {
  B01001,
  B00101,
  B00000,
  B01101,
  B00000,
  B00101,
  B01001,
  B00000
};
byte rdv2[] = {
  B01000,
  B10000,
  B00000,
  B11000,
  B00000,
  B10000,
  B01000,
  B00000
};

int referencia = 0;
bool upPressed = false;
bool downPressed = false;

// trechos = {8, 10, 11}
byte trechos[] = {8, 18, 29};

// 260, 140, 180, 155, 355, 45, 80, 30
// 75, 75, 20, 60, 330, 325, 280, 275, 20, 280
// 280, 15, 315, 25, 380, 5, 310, 300, 5, 20, 280

int graus[REFERENCIAS] = {260, 140, 180, 155, 355, 45, 80, 30, 75, 75, 20, 60, 330, 325, 280, 275, 20, 280, 280, 15, 315, 25, 380, 5, 310, 300, 5, 20, 280};

// sigaA = true, rosa dos ventos = false
// true, true, true, true, true, true, false, true,
// true, true, true, true, true, true, true, true, true, false
// false, true, true, true, true, true, true, true, true, true, false
bool sigaA[REFERENCIAS] = {true, true, true, true, true, true, false, true, true, true, true, true, true, true, true, true, true, false, false, true, true, true, true, true, true, true, true, true, false};

void setup() {
  lcd.begin(16, 2);

  lcd.createChar(0, grau);
  lcd.createChar(1, rdv1);
  lcd.createChar(2, rdv2);

  pinMode(8, INPUT_PULLUP);
  pinMode(9, INPUT_PULLUP);

  ponteiro.attach(10);
  Serial.begin(9600);
}

void loop() {

  int trecho = 0;
  int i = 0;
  while (trecho == 0) {
    Serial.print(referencia);
    Serial.print(" < ");
    Serial.print(trechos[i]);

    if (referencia < trechos[i]) {
      trecho = i + 1;
    }
    i++;
  }
  Serial.print(trecho);

  lcd.setCursor(0, 0);
  lcd.print("Trecho ");
  lcd.print(trecho);
  lcd.setCursor(0, 1);

  if (sigaA[referencia]) {
    lcd.print("Siga a ");
    lcd.print(graus[referencia]);
    lcd.write(byte(0));
  } else {
    lcd.write(byte(1));
    lcd.write(byte(2));
    lcd.print(graus[referencia]);
    lcd.write(byte(0));
  }

  delay(100);

  int sensorUp = digitalRead(8);
  int sensorDown = digitalRead(9);

  if (!upPressed && sensorUp == LOW) {
    if (referencia < REFERENCIAS) {
      referencia++;
      lcd.clear();
    }
    upPressed = true;
  }
  if (upPressed && sensorUp == HIGH) {
    upPressed = false;
  }

  if (!downPressed && sensorDown == LOW) {
    if (referencia > 0) {
      referencia--;
      lcd.clear();
    }
    downPressed = true;
  }
  if (downPressed && sensorDown == HIGH) {
    downPressed = false;
  }

  byte angulo = map(graus[referencia], 0, 380, 0, 180);
  ponteiro.write(angulo);
}
